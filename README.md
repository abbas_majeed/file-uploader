# File Uploading Using Node.Js, MYSQL Database & Angular 7 UI

Steps:
	
1 - create database e.g. compressed_files_db
	
2 - create table name `compressed_files` with columns `id` and `name` OR just import database file e.g. "compressed_files.sql"
	
3 - Run command 
		
	1 - npm install
		
	2 - npm index.js
	
4 - Call following API to upload files
		
	URL : http://localhost:8282/upload-files
		
	Request Body : {
			Method = POST
			Body-Type = form-data
			Param Name = files
			Param Type = files
		}

5 - URL for listing
		URL : http://localhost:8282/listing


6 - Download file
		URL : http://localhost:8282/download/{file-name}










This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 7.3.8.

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `--prod` flag for a production build.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI README](https://github.com/angular/angular-cli/blob/master/README.md).
