import { AbstractControl } from '@angular/forms';
export function MobileNumberUAEValidator(control: AbstractControl) {
    // if (!control.value.startsWith('https') || !control.value.includes('.io')) {
    //   return { validUrl: true };
    // }
    // return null;
    debugger;
    if (control.value != "" && control.value != undefined) {
        const NUMBER_REGEXP = /^(?:971|00971|0)?(?:50|51|52|55|56|2|3|4|6|7|9)\d{7}$/;
        //control.value.markAsTouched();
        if (NUMBER_REGEXP.test(control.value)) {
            return null;
        }
        return { validMobileNumber: true };
    }
    else {
        return null;
    }
}
