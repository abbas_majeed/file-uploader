
import { ValidationErrors, ValidatorFn, AbstractControl, FormArray } from '@angular/forms';

export class CustomValidator {

    // Validates URL
    static urlValidator(url: { pristine: any; markAsTouched: () => void; value: string; }): any {
        if (url.pristine) {
            return null;
        }
        const URL_REGEXP = /^(http?|ftp):\/\/([a-zA-Z0-9.-]+(:[a-zA-Z0-9.&%$-]+)*@)*((25[0-5]|2[0-4][0-9]|1[0-9]{2}|[1-9][0-9]?)(\.(25[0-5]|2[0-4][0-9]|1[0-9]{2}|[1-9]?[0-9])){3}|([a-zA-Z0-9-]+\.)*[a-zA-Z0-9-]+\.(com|edu|gov|int|mil|net|org|biz|arpa|info|name|pro|aero|coop|museum|[a-zA-Z]{2}))(:[0-9]+)*(\/($|[a-zA-Z0-9.,?'\\+&%$#=~_-]+))*$/;
        url.markAsTouched();
        if (URL_REGEXP.test(url.value)) {
            return null;
        }
        return {
            invalidUrl: true
        };
    }

    // Validates passwords
    static passwordConfirmingValidator(c: AbstractControl): any {
        if (!c.parent || !c) return;
        const pwd = c.parent.get('Password');
        const cpwd = c.parent.get('confirmPassword');

        if (!pwd || !cpwd || !pwd.value || !cpwd.value) return;

        if (pwd.value !== cpwd.value) {
            return { invalidPasswordConfirmingValidator: true };
        }
    }

    // Validates password
    static passwordValidator(number: { pristine: any; markAsTouched: () => void; value: string; }): any {

        if (number.pristine || !number.value) {
            return null;
        }

        const NUMBER_REGEXP = /^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[$@$!%*?&])[A-Za-z\d$@$!%*?&]{7,10}/
        number.markAsTouched();
        if (NUMBER_REGEXP.test(number.value)) {
            return null;
        }
        return {
            invalidPassword: true
        };
    }

    // Validates numbers
    static mobileNumberUAEValidator(number: { pristine: any; markAsTouched: () => void; value: string; }): any {
        if (number.pristine) {
            return null;
        }
        const NUMBER_REGEXP = /^(?:971|00971|0)?(?:50|51|52|55|56|2|3|4|6|7|9)\d{7}$/;
        number.markAsTouched();
        if (NUMBER_REGEXP.test(number.value)) {
            return null;
        }
        return {
            invalidNumber: true
        };
    }

    static multipleCheckboxRequireOneValidator(fa: FormArray) {
        let valid = false;

        for (let x = 0; x < fa.length; ++x) {
            if (fa.at(x).value) {
                valid = true;
                break;
            }
        }
        return valid ? null : {
            multipleCheckboxRequireOne: true
        };
    }

    static EmiratesIdValidator(number: { pristine: any; markAsTouched: () => void; value: string; }): any {
        ;
        if (number.pristine) {
            return null;
        }
        //784-1989-5276810-3
        const NUMBER_REGEXP = /^(?:784)?(?:50|51|52|55|56|2|3|4|6|7|9)\d{14}$/;
        number.markAsTouched();
        if (NUMBER_REGEXP.test(number.value)) {
            return null;
        }
        return {
            invalidNumber: true
        };
    }

    // Validates US SSN
    static ssnValidator(ssn: { pristine: any; markAsTouched: () => void; value: string; }): any {
        if (ssn.pristine) {
            return null;
        }
        const SSN_REGEXP = /^(?!219-09-9999|078-05-1120)(?!666|000|9\d{2})\d{3}-(?!00)\d{2}-(?!0{4})\d{4}$/;
        ssn.markAsTouched();
        if (SSN_REGEXP.test(ssn.value)) {
            return null;
        }
        return {
            invalidSsn: true
        };
    }

    // Validates US phone numbers
    static phoneValidator(number: { pristine: any; markAsTouched: () => void; value: string; }): any {
        if (number.pristine) {
            return null;
        }
        const PHONE_REGEXP = /^(\([0-9]{3}\) |[0-9]{3}-)[0-9]{3}-[0-9]{4}$/;
        number.markAsTouched();
        if (PHONE_REGEXP.test(number.value)) {
            return null;
        }
        return {
            invalidNumber: true
        };
    }

    // Validates zip codes
    static zipCodeValidator(zip: { pristine: any; markAsTouched: () => void; value: string; }): any {
        if (zip.pristine) {
            return null;
        }
        const ZIP_REGEXP = /^[0-9]{5}(?:-[0-9]{4})?$/;
        zip.markAsTouched();
        if (ZIP_REGEXP.test(zip.value)) {
            return null;
        }
        return {
            invalidZip: true
        };
    }

    static numberOnlyValidator(number: { pristine: any; markAsTouched: () => void; value: string; }): any {
        if (number.pristine) {
            return null;
        }
        const URL_REGEXP = /^[0-9]/;
        number.markAsTouched();
        if (URL_REGEXP.test(number.value)) {
            return null;
        }
        return {
            invalidNumber: true
        };
    }

    static patternValidator(regex: RegExp, error: ValidationErrors): ValidatorFn {
        return (control: AbstractControl): { [key: string]: any } => {
            if (!control.value) {
                // if control is empty return no error
                return null;
            }

            // test the value of the control against the regexp supplied
            const valid = regex.test(control.value);

            // if true, return no error (no error), else return error passed in the second parameter
            return valid ? null : error;
        };
    }

    static passwordMatchValidator(control: AbstractControl) {
        const password: string = control.get('password').value; // get password from our password form control
        const confirmPassword: string = control.get('confirmPassword').value; // get password from our confirmPassword form control
        // compare is the password math
        if (password !== confirmPassword) {
            // if they don't match, set an error in our confirmPassword form control
            control.get('confirmPassword').setErrors({ NoPassswordMatch: true });
        }
    }
}
