// import { Pipe, PipeTransform, Injectable } from '@angular/core';
// import { AppTranslateService } from 'src/app/service/core/app-translate.service';

// @Pipe({
//   name: 'localizeString'
// })

// export class LocalizeStringPipe implements PipeTransform {

//   constructor(private appTranslateService: AppTranslateService) {

//   }

//   transform(value: any, displayValue?: string): any {
//     if (value) {
//       if (displayValue) {
//         return value[displayValue];
//       }
//       if (this.appTranslateService.IsRTL()) {
//         return value.nameAr;
//       }
//       else {
//         return value.nameEn;
//       }
//     }
//   }

// }
