import { Injectable } from '@angular/core';
import swal from 'sweetalert2';

@Injectable({
  providedIn: 'root'
})
export class SwalService {

  constructor() { }

  tosterError(messageError: string) {
    const toast = swal.mixin({
      toast: true,
      position: 'bottom-end',
      showConfirmButton: false,
      timer: 5000
    });

    // toast({
    //   type: 'error',
    //   title: messageError
    // })
  }
  defulatSwal(type: any, title: string, message: string) {
    // swal({
    //   type: type,
    //   title: title,
    //   text: message
    // })
  }

  SwalSuccess(title: string, message: string) {
    // swal({
    //   type: 'success',
    //   title: title,
    //   text: message
    // })
  }

  SwalError(title: string, message: string, html?: string) {
    // swal({
    //   type: 'error',
    //   title: title,
    //   text: message,
    //   html: html
    // })
  }
}
