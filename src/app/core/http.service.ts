import { Response } from '@angular/http';
import { HttpClient, HttpParams, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { map } from 'rxjs/operators';
import { Observable } from 'rxjs';
import { ResponseData } from '../model/response.model';


@Injectable({
    providedIn: 'root'
})

export class HttpService {

    constructor(private http: HttpClient) { }

    public get(url: string, param: any = null): Observable<ResponseData> {
        return this.http.get(url, { params: param }).pipe(map(resp => resp as ResponseData));
    }

    public post(url: string, params: any = null): Observable<ResponseData> {
        return this.http.post(url, params).pipe(map(resp => resp as ResponseData));
    }
}
