import { Injectable } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';

@Injectable({
  providedIn: 'root'
})
export class AppTranslateService {

  constructor(private translate: TranslateService) { }

  public getTranslate(key: string | string[]): string {
    var result: string;
    this.translate.get(key).subscribe(res => {
      result = res;
    });
    return result;
  }

  public setlang(value: string) {
    this.translate.use(value);
  }


  public IsRTL() :boolean {
    return this.translate.currentLang == 'ar'
  }

}
