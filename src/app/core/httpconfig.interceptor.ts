import { Injectable } from '@angular/core';
import { HttpInterceptor, HttpRequest, HttpResponse, HttpHandler, HttpEvent, HttpErrorResponse } from '@angular/common/http';
import { Observable, throwError, from } from 'rxjs';
import { map, catchError } from 'rxjs/operators';
import { CookiesStorage } from './cookies-storage.service';
import { SwalService } from './swal.service';
import { NgxSpinnerService } from 'ngx-spinner';
import { AppTranslateService } from './app-translate.service';


@Injectable({
    providedIn: 'root'
})

export class HttpConfigInterceptor implements HttpInterceptor {

    constructor(
        private cookiesStorage: CookiesStorage,
        private swalService: SwalService,
        private spinner: NgxSpinnerService,
        private appTranslateService: AppTranslateService
    ) { }


    intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        const token: string = this.cookiesStorage.getCookie('accessToken');

        if (token) {
            request = request.clone({ headers: request.headers.set('Authorization', 'Bearer ' + token) });
        }

        if (!request.headers.has('Content-Type')) {
            request = request.clone({ headers: request.headers.set('Content-Type', 'application/json') });
        }

        request = request.clone({ headers: request.headers.set('Accept', 'application/json') });
        //   this.spinner.show();

        return next.handle(request).pipe(
            map((event: HttpEvent<any>) => {
                if (event instanceof HttpResponse) {
                    this.spinner.hide();
                    //console.log('HttpInterceptor -->> vent--->>>', event);
                }
                return event;
            }),
            catchError(this.handleError('', [])))
    }


    private handleError<T>(operation = 'operation', result?: any) {
        return (error: any): Observable<any> => {
            switch (error.status) {

                case 401: {
                    this.swalService.tosterError("Unauthorized");
                    break;
                }
                case 404: {
                    this.swalService.tosterError("Not Found");
                    break;
                }

                case 500: {
                    this.swalService.tosterError("Internal Server Error");
                    break;
                }

                case 412: {
                    let keys = Object.keys(error.error);
                    let errorMessage = ""
                    for (let prop of keys) {
                        errorMessage += "<div>" + error.error[prop].errors[0].errorMessage + '</div> <br/>';
                    }
                    this.swalService.SwalError(this.appTranslateService.getTranslate("general.error"), null, errorMessage)
                }
            }
            console.error('HttpInterceptor -->> ERROR --->>: ', error);
            this.spinner.hide();
            return throwError(error.error);

        };
    }
}