import { ResponseData } from '../Model/response.model';
import { Injectable } from '@angular/core';
import { HttpService } from './http.service';
import { Observable } from 'rxjs';
import { SettingService } from './settingService';
import { HttpHeaders, HttpClient } from '@angular/common/http';
import { Headers,RequestOptions, RequestMethod } from '@angular/http';

@Injectable({
  providedIn: 'root'
})

export class AttachmentService {

  constructor(private http: HttpClient, private httpService: HttpService, private settingService: SettingService) { }

  // Get Attachment List
  getAllAttachmentsList(): any {
    return this.httpService.get(this.settingService.baseUrl + "/listing");
  }

  // Download Attachment
  downloadAttachment(attachmentName:string): any {
    return this.httpService.get(this.settingService.baseUrl + "/download/" + attachmentName);
  }

  // Upload Files
  uploadAttachments(attachmentFiles: any): any {
    const file = attachmentFiles[0];

    const formData = new FormData();
    formData.append('files', file,file.name);
    formData.set('Content-type', 'multipart/form-data');
    formData.set('Accept', '/');

  
    // this.http.post(this.settingService.baseUrl + "/upload-files", formData,{ headers: new HttpHeaders({ 'Content-Type': 'multipart/form-data; boundary=..' , 'Accept':'*/*'})})
    const headers1 = new HttpHeaders(
      { 'Content-Type': 'multipart/form-data; boundary=..', 'Accept-Encoding': 'gzip, deflate', 'Accept': '*/*' }
    );
    this.http.post(this.settingService.baseUrl + "/upload-files", formData, { headers: headers1 })
      .subscribe(response => {
        //handle response
        return response;
      }, err => {
        //handle error
      });
  }
}
