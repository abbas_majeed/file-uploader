import { CustomValidator } from './shared/Global/custom-validator';
import { Observable, Subject } from 'rxjs';
import { AttachmentService } from './Services/AttachmentService'; // Attachment Service
import { Component, OnInit, OnDestroy } from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormControl, AbstractControl } from '@angular/forms';
import { SettingService } from './Services/settingService'; // Have Base URL for whole Application
import { HttpEvent, HttpParams, HttpRequest, HttpClient, HttpEventType, HttpResponse } from '@angular/common/http';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'File Uploading | Abbas - Assignment';
  attachmentsList : any[];
  uploadAttachemnts : any;

  dtOptions: DataTables.Settings = {};
  // We use this trigger because fetching the list of persons can be quite long,
  // thus we ensure the data is fetched before rendering
  dtTrigger: Subject<any> = new Subject();


  constructor(private AttachmentService: AttachmentService ,private settingService: SettingService, private formBuilder: FormBuilder,private http: HttpClient) {

   }

   //File Upload input Change Event
   selectFile(event) {
    this.uploadFile(event.target.files);
  }
   // file from event.target.files[0]
   uploadFile(files: FileList) {
    if (files.length == 0) {
      console.log("No file selected!");
      return

    }
    //let file: File = files[0];

    this.AttachmentService.uploadAttachments(files)
      .subscribe(
        event => {
          if (event.type == HttpEventType.UploadProgress) {
            const percentDone = Math.round(100 * event.loaded / event.total);
            console.log(`File is ${percentDone}% loaded.`);
          } else if (event instanceof HttpResponse) {
            console.log('File is completely loaded!');
          }
        },
        (err) => {
          console.log("Upload Error:", err);
        }, () => {
          console.log("Upload done");
        }
      )
  }

   ngOnInit() {

    //Initilize Datatable options
    this.dtOptions = {
      pagingType: 'full_numbers',
      pageLength: 10
    };

    // Get All Attachment List
    this.AttachmentService.getAllAttachmentsList().subscribe((res) => {
      this.attachmentsList = res.data;
       // Calling the DT trigger to manually render the table
       this.dtTrigger.next();
   });
  }

  // Donload Attachment
   public dowonloadAttachment(attachmentName) {
    debugger;
      this.AttachmentService.downloadAttachment(attachmentName).subscribe((res) => {
        debugger;
      });
    }
    
    // Add File on click
   public addFiles() {
    debugger;
      if(this.uploadAttachemnts){
        this.AttachmentService.uploadAttachments(this.uploadAttachemnts).subscribe((res) => {
          debugger;
          this.closePopup();

          this.AttachmentService.getAllAttachmentsList().subscribe((res) => {
            this.attachmentsList = res.data;
            // Calling the DT trigger to manually render the table
            this.dtTrigger.next();
        });
        });
      }
    }

    //Get Attachment link to download
    public getTheDownloadLink(attachmentName){
      return this.settingService.baseUrl +  "/download/" + attachmentName;
    }

    // Show Attachment Upload Popup Modal
    public openPopup() {
      document.getElementById('openModalPopup').style.display = 'block';
      document.getElementById('closeModalPopup').style.display = 'block';
  }

  // Hide Attachment Upload Popup Modal
  public closePopup() {
      document.getElementById('openModalPopup').style.display = 'none';
      document.getElementById('closeModalPopup').style.display = 'none';
  }
}
