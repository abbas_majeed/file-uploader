-- phpMyAdmin SQL Dump
-- version 4.9.0.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Sep 29, 2019 at 09:19 PM
-- Server version: 10.4.6-MariaDB
-- PHP Version: 7.3.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `compressed_files_db`
--

-- --------------------------------------------------------

--
-- Table structure for table `compressed_files`
--

CREATE TABLE `compressed_files` (
  `id` int(11) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `created_on` timestamp NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `compressed_files`
--

INSERT INTO `compressed_files` (`id`, `name`, `created_on`) VALUES
(6, '1569581953152.zip', '2019-09-26 17:22:36'),
(7, '1569582077411.zip', '2019-09-28 17:22:36'),
(8, '1569618695908.zip', '2019-09-29 17:22:36'),
(9, '1569784590561.zip', '2019-09-29 19:16:30');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `compressed_files`
--
ALTER TABLE `compressed_files`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `compressed_files`
--
ALTER TABLE `compressed_files`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
