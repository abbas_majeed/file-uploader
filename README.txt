Steps:
	
1 - create database e.g. compressed_files_db
	
2 - create table name `compressed_files` with columns `id` and `name` OR just import database file e.g. "compressed_files.sql"
	
3 - Run command 
		
	1 - npm install
		
	2 - npm index.js
	
4 - Call following API to upload files
		
	URL : http://localhost:8282/upload-files
		
	Request Body : {
			Method = POST
			Body-Type = form-data
			Param Name = files
			Param Type = files
		}

5 - URL for listing
		URL : http://localhost:8282/listing


6 - Download file
		URL : http://localhost:8282/download/{file-name}
