//Server Settings
const express = require('express');
const fileUpload = require('express-fileupload');
const cors = require('cors');
const bodyParser = require('body-parser');
const morgan = require('morgan');
const _ = require('lodash');

const app = express();

const path = require('path');
const fs = require('fs');
const archiver = require('archiver');
const findRemoveSync = require('find-remove')

// DB Connection . Files in lib folder
var mysql = require('mysql');
var connection = require('./lib/db');


// enable files upload
app.use(fileUpload({
    createParentPath: true
}));

//add other middleware
app.use(cors());
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(morgan('dev'));

//start app 
const port = process.env.PORT || 8282;
const serverAddress = 'http://localhost:' + port;

app.listen(port, () =>
    console.log(`App is listening on port ${port}.`)
);

//upload-files Web Service
app.post('/upload-files', async(req, res) => {
    try {
        if (!req.files) {
            res.send({
                status: false,
                message: 'No file uploaded'
            });
        } else {
            let uploadedFiles = [];
            const filesToZip = [];

            //loop all files
            _.forEach(_.keysIn(req.files.files), (key) => {
                let photo = req.files.files[key];

                //move photo to temp directory
                photo.mv('./temp/' + photo.name);

                filesToZip.push(__dirname + '/temp/' + photo.name);

                //push file details
                uploadedFiles.push({
                    name: photo.name,
                    mimetype: photo.mimetype,
                    size: photo.size
                });
            });


            try {

                // create a file to stream archive data to.
                var zipFileName = new Date().getTime() + ".zip";
                var output = fs.createWriteStream(__dirname + '/uploaded-files/' + zipFileName);
                var archive = archiver('zip', {
                    zlib: { level: 9 } // Sets the compression level.
                });

                // listen for all archive data to be written
                // 'close' event is fired only when a file descriptor is involved
                output.on('close', function() {
                    console.log(archive.pointer() + ' total bytes');
                    console.log('archiver has been finalized and the output file descriptor has closed.');
                });

                // This event is fired when the data source is drained no matter what was the data source.
                // It is not part of this library but rather from the NodeJS Stream API.
                // @see: https://nodejs.org/api/stream.html#stream_event_end
                output.on('end', function() {});

                // good practice to catch warnings (ie stat failures and other non-blocking errors)
                archive.on('warning', function(err) {
                    if (err.code === 'ENOENT') {
                        // log warning
                    } else {
                        // throw error
                        throw err;
                    }
                });

                // good practice to catch this error explicitly
                archive.on('error', function(err) {
                    throw err;
                });

                // pipe archive data to the file
                archive.pipe(output);

                //this is the streaming magic
                archive.pipe(res);

                for (const i in filesToZip) {
                    archive.file(filesToZip[i], { name: path.basename(filesToZip[i]) });
                }


                archive.finalize();
            } catch (error) {
                // something here
                console.log(error);
            }

            //Start Database insertion
            var zipFileObj = { name: zipFileName }
            connection.query('INSERT INTO compressed_files SET ?', zipFileObj, function(err, result) {
                //if(err) throw err
                if (err) {
                    console.log(err)
                } else {
                    console.log('success', 'Data added successfully!');
                }
            });
            //End Database

            //return response
            res.send({
                status: true,
                message: 'Files are uploaded',
                data: { name: zipFileName, url: serverAddress + '/download/' + zipFileName }
            });
        }
    } catch (err) {
        res.status(500).send(err);
    }
});

//Attachment Listing Web Service
app.get('/listing', function(req, res, next) {
    connection.query('SELECT * FROM compressed_files ORDER BY id DESC', function(err, rows, fields) {
        if (err) throw err

        // if result not found
        if (rows.length <= 0) {
            res.send({
                status: true,
                message: 'You don\'t have any zip file(s).',
            });
        } else {
            res.send({
                title: 'Total files : ' + rows.length,
                data: rows
            });
        }
    })
});

// Attachment Download Web Service
app.get('/download/:file', function(req, res, next) { // this routes all types of file
    var filePath = __dirname + "/uploaded-files/" + req.params.file;

    //file exists
    res.download(filePath);

    //return response
    res.send({
        status: true,
        message: 'File downloaded'
    });

});